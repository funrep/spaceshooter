from kivy.app import App
from kivy.core.window import Window
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty
from kivy.clock import Clock
from kivy.vector import Vector

import math

SPEED = 100
SPEED2 = 500

class Sprite(Image):
    vel_x = NumericProperty(0)
    vel_y = NumericProperty(0)

    vel = ReferenceListProperty(vel_x, vel_y)

    top = 0
    buttom = 0
    left = 0
    right = 0

    alive = True

    def __init__(self, *args, **kwargs):
        Image.__init__(self, *args, **kwargs)
        top = self.y + self.height
        buttom = self.y
        left = self.x
        right = self.x + self.width

    def update(self):
        pass

    def move(self):
        self.pos = Vector(*self.vel) + self.pos

    def intersect(self, sprite):
        return not ((self.left > sprite.right)
                    or (self.right < sprite.left)
                    or (self.top < sprite.bottom)
                    or (self.bottom > sprite.top))

    def collides(self, sprites):
        lst = []
        for sprite in sprites:
            if (self.intersect(sprite)):
                lst.append(sprite)
        return lst

class Player(Sprite):
    def update(self, dt, keys):
        self.move()
        if 'w' in keys:
            self.vel_y = SPEED * dt

        if 's' in keys:
            self.vel_y = -1 * SPEED * dt

        if 'a' in keys:
            self.vel_x = -1 * SPEED * dt

        if 'd' in keys:
            self.vel_x = SPEED * dt

        if keys == set([]):
            self.vel = [0, 0]


class Bullet(Sprite):
    angle = 0

    def __init__(self, start, dest, **kwargs):
        self.angle = math.atan2(dest[1]-start[1], dest[0]-start[0])
        self.pos = start
        Sprite.__init__(self, **kwargs)

    def update(self, dt):
        self.move()
        self.is_dead()
        self.vel_x = math.cos(self.angle) * SPEED2 * dt
        self.vel_y = math.sin(self.angle) * SPEED2 * dt

    def is_dead(self):
        if self.left > Window.width or self.buttom > Window.height:
            self.alive = False
        if self.top < 0 or self.right < 0:
            self.alive = False

class Game(Widget):
    fps = NumericProperty(60)
    keys = set()
    win = Window.width, Window.height

    player = ObjectProperty(None)
    bullets = []

    def update(self, dt):
        self.fps = round(Clock.get_fps())
        self.player.update(dt, self.keys)

        for bullet in self.bullets:
            bullet.update(dt)
            if bullet.alive == False:
                self.remove_widget(bullet)
                self.bullets.remove(bullet)

        print self.keys

    def on_touch_down(self, touch):
        bullet = Bullet(self.player.pos, touch.pos)
        self.bullets.append(bullet)
        self.add_widget(bullet)
        
    def __init__(self, **kwargs):
        super(Game, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down = self._on_keyboard_down)
        self._keyboard.bind(on_key_up = self._on_keyboard_up)

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down = self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_up(self, *args):
        self.keys.discard(args[1][1])

    def _on_keyboard_down(self, *args):
        self.keys.add(args[1][1])

class GameApp(App):
    def build(self):
        game = Game()
        Clock.schedule_interval(game.update, 1.0 / 60.0)
        return game

if __name__ == '__main__':
    GameApp().run()
